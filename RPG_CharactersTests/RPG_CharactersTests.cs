using RPG_Characters.Attributes;
using RPG_Characters.Exceptions;
using RPG_Characters.Heroes;
using RPG_Characters.Items;
using Xunit;

namespace RPG_CharactersTests.RPG_CharactersTests
{
    public class RPG_CharactersTests
    {
        #region Constructor
        [Fact]
        public void Constructor_CreateMage_ShouldHaveRightValues()
        {
            // Arrange + Act
            Mage mage = new Mage("Harry Potter");

            // Assert
            Assert.Equal("Harry Potter", mage.Name);
            Assert.Equal(1, mage.Level);
            Assert.Equal(1, mage.LevelAttribute.Strength);
            Assert.Equal(1, mage.LevelAttribute.Dexterity);
            Assert.Equal(8, mage.LevelAttribute.Intelligence);
        }

        [Fact]
        public void Constructor_CreateRanger_ShouldHaveRightValues()
        {
            // Arrange + Act
            Ranger ranger = new Ranger("Lone Ranger");

            // Assert
            Assert.Equal("Lone Ranger", ranger.Name);
            Assert.Equal(1, ranger.Level);
            Assert.Equal(1, ranger.LevelAttribute.Strength);
            Assert.Equal(7, ranger.LevelAttribute.Dexterity);
            Assert.Equal(1, ranger.LevelAttribute.Intelligence);
        }

        [Fact]
        public void Constructor_CreateRouge_ShouldHaveRightValues()
        {
            // Arrange + Act
            Rouge rouge = new Rouge("Tom");

            // Assert
            Assert.Equal("Tom", rouge.Name);
            Assert.Equal(1, rouge.Level);
            Assert.Equal(2, rouge.LevelAttribute.Strength);
            Assert.Equal(6, rouge.LevelAttribute.Dexterity);
            Assert.Equal(1, rouge.LevelAttribute.Intelligence);
        }

        [Fact]
        public void Constructor_CreateWarrior_ShouldHaveRightValues()
        {
            // Arrange + Act
            Warrior warrior = new Warrior("Dave");

            // Assert
            Assert.Equal("Dave", warrior.Name);
            Assert.Equal(1, warrior.Level);
            Assert.Equal(5, warrior.LevelAttribute.Strength);
            Assert.Equal(2, warrior.LevelAttribute.Dexterity);
            Assert.Equal(1, warrior.LevelAttribute.Intelligence);
        }


        [Theory]
        [InlineData("Mark")]
        [InlineData("Michael")]
        [InlineData("Scott")]
        public void Constructor_CheckNames_NameIsOkay(string name)
        {
            // Arrange
            Rouge rouge = new(name);

            // Act + Assert
            Assert.Equal(rouge.Name, name);
        }
        #endregion


        #region LevelUp
        [Fact]
        public void LevelUp_IncreaseLevelByOne_ShouldBeOneHigher() 
        {
            // Arrange
            Mage mage = new Mage("Mark");
            int prevLevel = mage.Level;

            // Act
            mage.LevelUp(); 

            // Arrange
            int newLevel = mage.Level;

            // Assert
            Assert.True(prevLevel + 1 == newLevel);
        }
        [Fact]
        public void LevelUp_IncreaseLevelByTwo_ShouldBeTwoHigher()
        {
            // Arrange
            Mage mage = new Mage("Tom");
            int prevLevel = mage.Level;

            // Act
            mage.LevelUp();
            mage.LevelUp();

            // Arrange
            int newLevel = mage.Level;

            // Assert
            Assert.True(prevLevel + 2 == newLevel);
        }
        #endregion

        #region EquipArmor
        [Fact]
        public void EquipArmor_EquipValidArmor_ShouldAddToEquipment()
        {            
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common mail";
            int RequiredLevel = 0;
            Slot slot = Slot.Body;
            ArmorType armorType = ArmorType.Mail;
            int strength = 4; 
            int dexterity = 5;
            int intelligence = 6;

            Armor armor = new Armor(Itemname, RequiredLevel, slot, armorType,
            strength, dexterity, intelligence);

            // Assert 
            bool isEquipped = ranger.Equipment[armor.slot] == armor;
            Assert.False(isEquipped);

            // Act
            ranger.EquipArmor(armor);

            // Assert
            isEquipped = ranger.Equipment[armor.slot] == armor;
            Assert.True(isEquipped);
        }

        [Fact]
        public void EquipArmor_ArmorTooHighLevel_ShouldThrowException()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common mail";
            int RequiredLevel = 9999; // Set to very high
            Slot slot = Slot.Body;
            ArmorType armorType = ArmorType.Mail;
            int strength = 4;
            int dexterity = 5;
            int intelligence = 6;

            Armor armor = new Armor(Itemname, RequiredLevel, slot, armorType,
            strength, dexterity, intelligence);

            // Assert 
            bool isEquipped = ranger.Equipment[armor.slot] == armor;
            Assert.False(isEquipped);

            // Act
            Assert.Throws<InvalidArmorException>(() => ranger.EquipArmor(armor));
        }

        [Fact]
        public void EquipArmor_ArmorNotCompatibleWithHero_ShouldThrowException()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common cloth";
            int RequiredLevel = 0;
            Slot slot = Slot.Body;
            ArmorType armorType = ArmorType.Cloth; // Changed this to cloth to cause exception 
            int strength = 4;
            int dexterity = 5;
            int intelligence = 6;

            Armor armor = new Armor(Itemname, RequiredLevel, slot, armorType,
            strength, dexterity, intelligence);

            // Assert 
            bool isEquipped = ranger.Equipment[armor.slot] == armor;
            Assert.False(isEquipped);

            // Act + Assert
            Assert.Throws<InvalidArmorException>(() => ranger.EquipArmor(armor));
        }
        #endregion


        #region EquipWeapon
        [Fact]
        public void EquipWeapon_EquipValidWeapon_ShouldAddToEquipment()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common bow";
            int RequiredLevel = 0;
            Slot slot = Slot.Weapon;
            int WeaponDamage = 3;
            WeaponType weaponType = WeaponType.Bow;

            Weapon weapon = new Weapon(Itemname, RequiredLevel, slot, weaponType, WeaponDamage);

            // Assert 
            bool isEquipped = ranger.Equipment[weapon.slot] == weapon;
            Assert.False(isEquipped);

            // Act
            ranger.EquipWeapon(weapon);

            // Assert
            isEquipped = ranger.Equipment[weapon.slot] == weapon;
            Assert.True(isEquipped);
        }

        [Fact]
        public void EquipWeapon_WeaponTooHighLevel_ShouldThrowException()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common bow";
            int RequiredLevel = 999; // Set to too high required level
            Slot slot = Slot.Weapon;
            int WeaponDamage = 3;
            WeaponType weaponType = WeaponType.Bow;

            Weapon weapon = new Weapon(Itemname, RequiredLevel, slot, weaponType, WeaponDamage);

            // Assert 
            bool isEquipped = ranger.Equipment[weapon.slot] == weapon;
            Assert.False(isEquipped);

            // Act + Assert
            Assert.Throws<InvalidWeaponException>(() => ranger.EquipWeapon(weapon));
        }

        [Fact]
        public void EquipWeapon_WeaponNotCompatibleWithHero_ShouldThrowException()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common axe";
            int RequiredLevel = 0;
            Slot slot = Slot.Weapon;
            int WeaponDamage = 3;
            WeaponType weaponType = WeaponType.Axe; // Set to axe to cause exception

            Weapon weapon = new Weapon(Itemname, RequiredLevel, slot, weaponType, WeaponDamage);

            // Assert 
            bool isEquipped = ranger.Equipment[weapon.slot] == weapon;
            Assert.False(isEquipped);

            // Act + Assert
            Assert.Throws<InvalidWeaponException>(() => ranger.EquipWeapon(weapon));
        }
        #endregion


        #region TotalAttributes
        [Fact]
        public void TotalAttributes_NoEquipment_ShouldEqualLevelAttributes()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");

            // Act + Assert
            Assert.Equal(ranger.LevelAttribute.Strength, ranger.TotalAttributes().Strength);
            Assert.Equal(ranger.LevelAttribute.Dexterity, ranger.TotalAttributes().Dexterity);
            Assert.Equal(ranger.LevelAttribute.Intelligence, ranger.TotalAttributes().Intelligence);   
        }

        [Fact]
        public void TotalAttributes_OneEquipment_ShouldEqualLevelAttributesPlusEquipment()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common mail";
            int RequiredLevel = 0;
            Slot slot = Slot.Body;
            ArmorType armorType = ArmorType.Mail;
            int strength = 4;
            int dexterity = 5;
            int intelligence = 6;

            Armor armor = new Armor(Itemname, RequiredLevel, slot, armorType,
            strength, dexterity, intelligence);

            ranger.EquipArmor(armor);

            // Act + Assert
            Assert.Equal(ranger.LevelAttribute.Strength+strength, ranger.TotalAttributes().Strength);
            Assert.Equal(ranger.LevelAttribute.Dexterity+dexterity, ranger.TotalAttributes().Dexterity);
            Assert.Equal(ranger.LevelAttribute.Intelligence+intelligence, ranger.TotalAttributes().Intelligence);
        }

        [Fact]
        public void TotalAttributes_FullEquipment_ShouldEqualSumOfAllAttributes()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common mail";
            int RequiredLevel = 0;
            ArmorType armorType = ArmorType.Mail;

            Weapon weapon = new Weapon("Bow", 0, Slot.Weapon, WeaponType.Bow, 2);
            ranger.EquipWeapon(weapon);

            Armor armor1 = new Armor(Itemname, RequiredLevel, Slot.Head, armorType, 1, 1, 1);
            ranger.EquipArmor(armor1);

            Armor armor2 = new Armor(Itemname, RequiredLevel, Slot.Legs, armorType, 4, 4, 4);
            ranger.EquipArmor(armor2);

            Armor armor3 = new Armor(Itemname, RequiredLevel, Slot.Body, armorType, 7, 7, 7);
            ranger.EquipArmor(armor3);


            // Act + Assert
            Assert.Equal(ranger.LevelAttribute.Strength+1+4+7, ranger.TotalAttributes().Strength);
            Assert.Equal(ranger.LevelAttribute.Dexterity+1+4+7, ranger.TotalAttributes().Dexterity);
            Assert.Equal(ranger.LevelAttribute.Intelligence+1+4+7, ranger.TotalAttributes().Intelligence);
        }
        #endregion

        #region CalculateDamage
        [Fact]
        public void CalculateDamage_WithoutWeapon_ShouldBeBasedOnPrimaryAttribute()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");

            // Act
            double damage = ranger.CalculateDamage();

            // Assert
            Assert.Equal(1 * (1 + (double) ranger.LevelAttribute.Dexterity / 100), damage);
        }

        [Fact]
        public void CalculateDamage_WithWeaponAndArmor_ShouldIncreaseDamageByWeaponDamage()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common bow";
            int RequiredLevel = 0;
            Slot slot = Slot.Weapon;
            int WeaponDamage = 3;
            WeaponType weaponType = WeaponType.Bow;
            Weapon weapon = new Weapon(Itemname, RequiredLevel, slot, weaponType, WeaponDamage);
            ranger.EquipWeapon(weapon);

            Itemname = "Common mail";
            RequiredLevel = 0;
            slot = Slot.Body;
            ArmorType armorType = ArmorType.Mail;
            int strength = 4;
            int dexterity = 5;
            int intelligence = 6;

            Armor armor = new Armor(Itemname, RequiredLevel, slot, armorType, strength, dexterity, intelligence);
            ranger.EquipArmor(armor);

            // Act
            double damage = ranger.CalculateDamage();

            // Assert
            Assert.Equal(WeaponDamage * (1 + (double) ranger.GetPrimaryAttribute() / 100), damage);
        }
        #endregion


        #region GetPrimaryAttribute
        [Fact]
        public void GetPrimaryAttribute_MageNoEquipment_ShouldBeLevelAttributesIntelligence()
        {
            // Arrange
            Mage mage = new Mage("Johnny");

            // Act + Assert
            Assert.Equal(mage.LevelAttribute.Intelligence, mage.GetPrimaryAttribute());
        }

        [Fact]
        public void GetPrimaryAttribute_RangerNoEquipment_ShouldBeLevelAttributesDexterity()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");

            // Act + Assert
            Assert.Equal(ranger.LevelAttribute.Dexterity, ranger.GetPrimaryAttribute());
        }

        [Fact]
        public void GetPrimaryAttribute_WarriorNoEquipment_ShouldBeLevelAttributesStrength()
        {
            // Arrange
            Warrior warrior = new Warrior("Johnny");

            // Act + Assert
            Assert.Equal(warrior.LevelAttribute.Strength, warrior.GetPrimaryAttribute());
        }

        [Fact]
        public void GetPrimaryAttribute_RougeNoEquipment_ShouldBeLevelAttributesDexterity()
        {
            // Arrange
            Rouge rouge = new Rouge("Johnny");

            // Act + Assert
            Assert.Equal(rouge.LevelAttribute.Dexterity, rouge.GetPrimaryAttribute());
        }

        [Fact]
        public void GetPrimaryAttribute_RangerWithEquipment_ShouldBeTotalDexterityIncludingDexterityOfArmor()
        {
            // Arrange
            Ranger ranger = new Ranger("Johnny");
            string Itemname = "Common mail";
            int RequiredLevel = 0;
            Slot slot = Slot.Body;
            ArmorType armorType = ArmorType.Mail;
            int strength = 4;
            int dexterity = 5;
            int intelligence = 6;

            Armor armor = new Armor(Itemname, RequiredLevel, slot, armorType, strength, dexterity, intelligence);
            ranger.EquipArmor(armor);

            // Act + Assert
            Assert.Equal(ranger.LevelAttribute.Dexterity + dexterity, ranger.GetPrimaryAttribute());

        }
        #endregion
    }
}