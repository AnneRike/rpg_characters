﻿using System;
using RPG_Characters.Game;

namespace RPG_Characters
{
    class Program
    {
        static void Main(string[] args)
        {
            GameController gameController = new GameController();
            gameController.Start();

        }
    }
}