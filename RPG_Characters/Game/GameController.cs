﻿using RPG_Characters.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Game
{
    public class GameController
    {
        public void Start()
        {
            Random rnd = new Random();

            // Take in Name from USER
            Console.WriteLine("Welcome! What is your name?: ");
            string name = Console.ReadLine();

            // Take in number from 1-4 from user
            Console.WriteLine("Choose your type of Hero:");
            Console.WriteLine("0. Mage, 1. Ranger, 2. Warrior, 3. Rouge");
            int inp = Convert.ToInt32(Console.ReadLine());

            // Create a Mage / Ranger / Warrior / Rouge based on number
            Hero hero = null;
            if (inp == 0)
            {
                hero = new Mage(name);
            }
            if (inp == 1)
            {
                hero = new Ranger(name);
            }
            if (inp == 2)
            {
                hero = new Warrior(name);
            }
            if (inp == 3)
            {
                hero = new Rouge(name);
            }

            hero.LevelUp();
            hero.LevelUp();
            hero.LevelUp();
            hero.LevelUp();
            hero.LevelUp();
            hero.DisplayHero();

            // Start Main While Loop
            while (true)
            {
                Console.WriteLine("0. Fight Hank, 1. Display, 2. Exit");
                inp = Convert.ToInt32(Console.ReadLine());
                if (inp == 0)
                {
                    Warrior warrior = new Warrior("Hank");
                    warrior.LevelAttribute.Strength = rnd.Next(1, 70);
                    warrior.LevelAttribute.Dexterity = rnd.Next(1, 70);
                    warrior.LevelAttribute.Intelligence = rnd.Next(1, 70);

                    Console.WriteLine("Hank says: Hey! You dare to challenge ME!?");
                    Console.WriteLine("*Hanks is showing off*");
                    warrior.DisplayHero();

                    Console.WriteLine("Press anything to Continue...");
                    Console.ReadLine();
                    Console.WriteLine("");

                    Console.WriteLine($"Your Damage {hero.CalculateDamage()}   Hank Damage: {warrior.CalculateDamage()}");

                    if (hero.CalculateDamage() >= warrior.CalculateDamage())
                    {
                        Console.WriteLine("You Won!");
                        Console.WriteLine("You level up!");
                        hero.LevelUp();
                    }
                    else
                    {
                        Console.WriteLine("You Lost! Hank is stronger than you");
                    }

                }
                if (inp == 1)
                {
                    hero.DisplayHero();
                }
                if (inp == 2)
                {
                    Console.WriteLine($"Thank you for playing {name}! Hope to see you again :)");
                    break;
                }
            }

            // 1. Fight, 2. Display, 3. Exit

            // Fight --> create random opponent

            // If won --> get random Item

            // If lost --> end

            // Display --> Display stats of Hero

            // Exit --> Exit

        }
    }
}
