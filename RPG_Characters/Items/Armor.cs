﻿using RPG_Characters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public class Armor : Item
    {
        public HeroAttribute ArmorAttribute { get; set; }
        public ArmorType armorType { get; set; }
        public Armor(string Itemname, int RequiredLevel, Slot slot, ArmorType armorType, 
            int strength, int dexterity, int intelligence) : base(Itemname, RequiredLevel, slot)
        {
            ArmorAttribute = new HeroAttribute(strength, dexterity, intelligence);

            this.armorType = armorType;  
        }
    }
    public enum ArmorType // Created enumerator to encapsulate types of armor 
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }


}
