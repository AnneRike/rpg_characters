﻿using RPG_Characters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public class Weapon : Item
    {
        public int WeaponDamage { get; set; } 
        public WeaponType weaponType { get; set; }

        public Weapon(string Itemname, int RequiredLevel, Slot slot, WeaponType WeaponType, 
            int WeaponDamage) : base(Itemname, RequiredLevel, slot)
        {
            this.WeaponDamage = WeaponDamage;  
            this.weaponType = WeaponType;
        }
    }
    public enum WeaponType // Created enumerator to encapsulate types of weapons 
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
}
