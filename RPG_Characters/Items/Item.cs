﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public abstract class Item
    {
        public string ItemName { get; set; } 
        public int RequiredLevel { get; set; } 
        public Slot slot { get; set; } 

        public Item(string Itemname, int RequiredLevel, Slot slot)
        {
            this.ItemName = ItemName;
            this.RequiredLevel = RequiredLevel;
            this.slot = slot; 
        }
    }
    public enum Slot // Created enumerator to encapsulate types of slots 
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}
