﻿using RPG_Characters.Attributes;
using RPG_Characters.Items;
using RPG_Characters.Exceptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RPG_Characters.Heroes
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public HeroAttribute LevelAttribute { get; set; }
        public HeroAttribute GainAttribute { get; set; } // The increase in strength, dexterity, intelligence when leveling 
        public Dictionary<Slot, Item> Equipment {get; set;} 
        public List<WeaponType> validWeaponTypes { get; set; } 
        public List<ArmorType> validArmorTypes { get; set; }

        public Hero(string name)
        { 
            // Default values for heroes
            Name = name;
            Level = 1;
            // Details are added in the respective subclasses constructors according to the assignment
            LevelAttribute = new HeroAttribute(0, 0, 0);
            GainAttribute = new HeroAttribute(0, 0, 0);
            validWeaponTypes = new List<WeaponType>(); 
            validArmorTypes = new List<ArmorType>();

            // Initializes empty Equipment to be filled later
            Equipment = new Dictionary<Slot, Item>() 
            {
                {Slot.Weapon, null },
                {Slot.Head, null },
                {Slot.Body, null },
                {Slot.Legs, null }
            };
        }

        // Total (Strength/ Dexterity/ Intelligence) depending on hero-subclass
        public abstract int GetPrimaryAttribute();

        // Get string describing hero-subclass
        public abstract string GetClass(); 


        /// <summary>
        /// Increases the level by 1 and adds the LevelAttribute of the Hero with how many 
        /// attributes they gain for Strength, Dexterity and Intelligence.
        /// </summary>
        public void LevelUp()
        {
            Level += 1;
            LevelAttribute.Strength += GainAttribute.Strength;
            LevelAttribute.Dexterity += GainAttribute.Dexterity;
            LevelAttribute.Intelligence += GainAttribute.Intelligence;
        }


        /// <summary>
        /// Checks if the armor is the wrong type or being too high a level requirement 
        /// for the type of hero. Updates the dictionary with the armor for the hero.
        /// </summary>
        /// <param name="armor"></param>
        /// <exception cref="InvalidArmorException">When is it the wrong type of armor for the type of hero</exception>
        /// <exception cref="InvalidArmorException">If the level of the hero is less than the required level to use the armor</exception>
        public void EquipArmor(Armor armor)
        {
            // Checks if it is not the right type of armor for the hero 
            if (!validArmorTypes.Contains(armor.armorType)) 
            {
                throw new InvalidArmorException($"The armor type is not valid for {GetClass()}");  
            }

            // Checks if the required level to use the armor is higher than the level of the hero
            if (armor.RequiredLevel > Level)
            {
                throw new InvalidArmorException($"The required level of this armor is too high. ({armor.RequiredLevel} > {Level})");
            }

            // Updates the dictionary with the armor that is allowed for the hero. Puts the armor in the right slot. 
            Equipment[armor.slot] = armor;
        }



        /// <summary>
        /// Checks if the weapon is the wrong type or being too high a level requirement for the type of hero. 
        /// Updates the dictionary with the weapon for the hero.
        /// </summary>
        /// <param name="weapon"></param>
        /// <exception cref="InvalidWeaponException">When it is the wrong type of weapon for the type of hero</exception>
        /// <exception cref="InvalidWeaponException">If the level of the hero is less than the required level to use the weapon</exception>
        public void EquipWeapon(Weapon weapon)
        {
            // Checks if it is the wrong type of weapon
            if (!validWeaponTypes.Contains(weapon.weaponType))
            {
                throw new InvalidWeaponException($"The weapon type is not valid for {GetClass()}");
            }
                
            // Checks if the required level to use the weapon is higher than the level of the hero
            if (weapon.RequiredLevel > Level)
            {
                throw new InvalidWeaponException($"The required level of this weapon is too high. ({weapon.RequiredLevel} > {Level})");
            }
            // Updates the dictionary with the weapon that is allowed for the hero. Puts the weapon in the right slot. 
            Equipment[Slot.Weapon] = weapon;
        }




        /// <summary>
        /// TotalAttributes (adds LevelAttributes and (loops over each armor and adds each armor attribute))
        /// </summary>
        /// <returns>A new HeroAttribute that is the sum of the of each type of attributes</returns>
        public HeroAttribute TotalAttributes()
        {
            // Initializes ResultAttribute
            HeroAttribute ResultAttribute = new HeroAttribute(0, 0, 0);

            // Uses the override of + operator for two hero attributes (see: HeroAttribute.cs)
            ResultAttribute += LevelAttribute;

            // Then we loop over each of our Items (in Equipment that is not of "slot-type" Weapon and is not null)
            foreach (KeyValuePair<Slot, Item> entry in Equipment)
            {
                if (entry.Key != Slot.Weapon && entry.Value != null)
                {
                    Armor armor = (Armor) entry.Value;

                    ResultAttribute += armor.ArmorAttribute;
                }
            }
            return ResultAttribute;
        }


        /// <summary>
        /// Calculates damage based on LevelAttributes, equipped armor(s) and eqipped weapon
        /// </summary>
        /// <returns>The calculated damage</returns>
        public double CalculateDamage()
        {
            int WeaponDamage;

            // Checks if a weapon is equipped
            if (Equipment[Slot.Weapon] != null)
            {
                Weapon weapon = (Weapon) Equipment[Slot.Weapon]; 

                WeaponDamage = weapon.WeaponDamage; 
            }
            else
            {
                WeaponDamage = 1; //NOTE: If a Hero has no weapon equipped, take their WeaponDamage to be 1.
            }

            double damagingAttribute = GetPrimaryAttribute(); // Found inside subclasses of Hero
            return WeaponDamage * (1 + damagingAttribute / 100);
        }



        // Displays details about the hero
        public void DisplayHero()
        {
            // Creates a stringbuilder to increase efficiency
            StringBuilder stringBuilderDisplay = new StringBuilder();

            //name
            stringBuilderDisplay.AppendLine($"Name: {Name}");

            //class
            stringBuilderDisplay.AppendLine($"Hero type: {GetClass()}");

            //level 
            stringBuilderDisplay.AppendLine($"Level: {Level}");

            //strength
            stringBuilderDisplay.AppendLine("Strength: " + LevelAttribute.Strength);

            //dexterity
            stringBuilderDisplay.AppendLine("Dexterity: " + LevelAttribute.Dexterity);

            //intelligence
            stringBuilderDisplay.AppendLine("Intelligence: " + LevelAttribute.Intelligence);

            //damage
            stringBuilderDisplay.AppendLine(String.Format("Damage: {0:0.000}", CalculateDamage()));

            string displayString = stringBuilderDisplay.ToString();
            Console.WriteLine(displayString);
        }
    }
}
