﻿using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Heroes
{
    public class Rouge : Hero
    {
        /// <summary>
        /// Initializes a Rouge object with correct values
        /// </summary>
        /// <param name="name">Name</param>
        public Rouge(string name) : base(name)
        {
            LevelAttribute.Strength = 2;
            LevelAttribute.Dexterity = 6;
            LevelAttribute.Intelligence = 1;

            GainAttribute.Strength = 1;
            GainAttribute.Dexterity = 4;
            GainAttribute.Intelligence = 1;

            validArmorTypes.Add(ArmorType.Leather);
            validArmorTypes.Add(ArmorType.Mail);

            validWeaponTypes.Add(WeaponType.Dagger);
            validWeaponTypes.Add(WeaponType.Sword);
        }
        public override int GetPrimaryAttribute() 
        {
            return TotalAttributes().Dexterity;
        }

        public override string GetClass()
        {
            return "Rouge";
        }
    }
}
