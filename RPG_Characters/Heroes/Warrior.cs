﻿using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Heroes
{
    public class Warrior : Hero
    {
        /// <summary>
        /// Initializes a Warrior object with correct values
        /// </summary>
        /// <param name="name">Name</param>
        public Warrior(string name) : base(name)
        {
            LevelAttribute.Strength = 5;
            LevelAttribute.Dexterity = 2;
            LevelAttribute.Intelligence = 1;

            GainAttribute.Strength = 3;
            GainAttribute.Dexterity = 2;
            GainAttribute.Intelligence = 1;

            validArmorTypes.Add(ArmorType.Mail);
            validArmorTypes.Add(ArmorType.Plate);

            validWeaponTypes.Add(WeaponType.Axe);
            validWeaponTypes.Add(WeaponType.Hammer);
            validWeaponTypes.Add(WeaponType.Sword);
        }
        public override int GetPrimaryAttribute() 
        {
            return TotalAttributes().Strength;
        }

        public override string GetClass()
        {
            return "Warrior";
        }
    }
}
