﻿using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Heroes
{
    public class Ranger : Hero
    {
        /// <summary>
        /// Initializes a Ranger object with correct values
        /// </summary>
        /// <param name="name">Name</param>
        public Ranger(string name) : base(name)
        {
            LevelAttribute.Strength = 1;
            LevelAttribute.Dexterity = 7;
            LevelAttribute.Intelligence = 1;

            GainAttribute.Strength = 1;
            GainAttribute.Dexterity = 5;
            GainAttribute.Intelligence = 1;

            validArmorTypes.Add(ArmorType.Leather);
            validArmorTypes.Add(ArmorType.Mail);

            validWeaponTypes.Add(WeaponType.Bow);
        }
        public override int GetPrimaryAttribute() 
        {
            return TotalAttributes().Dexterity;
        }

        public override string GetClass()
        {
            return "Ranger";
        }
    }
}
