﻿using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Heroes
{
    public class Mage : Hero
    {
        /// <summary>
        /// Initializes a Mage object with correct values
        /// </summary>
        /// <param name="name">Name</param>
        public Mage(string name) : base(name)
        {
            LevelAttribute.Strength = 1;
            LevelAttribute.Dexterity = 1;
            LevelAttribute.Intelligence = 8;

            GainAttribute.Strength = 1;
            GainAttribute.Dexterity = 1;
            GainAttribute.Intelligence = 5;

            validArmorTypes.Add(ArmorType.Cloth);

            validWeaponTypes.Add(WeaponType.Staff);
            validWeaponTypes.Add(WeaponType.Wand);

        }

        public override int GetPrimaryAttribute() 
        {
            return TotalAttributes().Intelligence;
        }

        public override string GetClass()
        {
            return "Mage";
        }

    }
}
