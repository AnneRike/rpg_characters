﻿using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Attributes
{
    public class HeroAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        /// <summary>
        /// Creates an overload operator using +.
        /// Method to add two HeroAttribute objects together
        /// </summary>
        /// <param name="a">HeroAttribute 1</param>
        /// <param name="b">HeroAttribute 2</param>
        /// <returns>New HeroAttribute which sums strength, dexterity and intelligence of a and b</returns>
        public static HeroAttribute operator +(HeroAttribute a, HeroAttribute b)
        {
            int ResStrength = a.Strength + b.Strength;
            int ResDexterity = a.Dexterity + b.Dexterity;
            int ResIntelligence = a.Intelligence + b.Intelligence;

            return new HeroAttribute(ResStrength, ResDexterity, ResIntelligence);
        }

    }
}
